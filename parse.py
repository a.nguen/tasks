import requests
from bs4 import BeautifulSoup as BS

r = requests.get('https://www.billboard.com/charts/hot-100')
html = BS(r.content, 'html.parser')

for el in html.select('.chart-list__element'):
	title = el.select('.chart-element__information__song')
	desc = el.select('.chart-element__information__artist')
	print( title[0].text + " - " + desc[0].text);